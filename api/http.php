<?php

use app\app\AppOne;

define('DIR_ROOT', __DIR__ . '/');
require_once DIR_ROOT . 'config/dir_define.php';
require_once DIR_CONFIG . 'php_modify.php';
require_once DIR_CONFIG . 'db_config.php';
require_once DIR_HELPER . 'global.php';

AppOne::run($_REQUEST['_uri']);

