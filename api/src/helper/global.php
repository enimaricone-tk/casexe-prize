<?php

function gPost()
{
    return json_decode(file_get_contents('php://input'), true);
}

function writeLog($data, $file = 'default')
{
    $path = DIR_LOGS . date("Y-m-d_") . $file . '.log';
    $str = PHP_EOL . $data . PHP_EOL;
    return file_put_contents($path, $str);
}