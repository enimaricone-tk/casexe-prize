<?php


namespace map;

use app\database\CRUD;
use app\database\DatabaseInterface;
use app\storage\Storage;
use app\storage\StorageSession;

abstract class AbstractMap
{
    protected $storage;

    /**
     * @var DatabaseInterface
     */
    protected $db;

    public function __construct()
    {
        $this->storage = Storage::getStorage();;
        $this->db = $this->storage->get('db');
        $this->user_id = StorageSession::get('user_id');
    }

    public function create($table) {
        $crud = new CRUD($table);
        return $crud->create(gPost());
    }

    public function read($table, $id) {
        $crud = new CRUD($table);
        return $crud->read($id);

    }

    public function update($table, $id) {
        $crud = new CRUD($table);
        return $crud->update($id, gPost());
    }

    public function delete($table, $id) {
        $crud = new CRUD($table);
        return $crud->delete($id);
    }

}