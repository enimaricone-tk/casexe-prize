<?php


namespace map;


use app\database\CRUD;
use prize\PrizeBase;

class QueuePrize extends AbstractMap
{
    const STATUS_NEW = 1;
    const STATUS_PROCESS = 2;
    const STATUS_CLOSE = 3;
    const STATUS_RETURN = 4;
    const STATUS_EXCEPTION = 5;

    public function process() {
        $query = "SELECT queue_prize_id, 
                  queue_prize_prize_type,
                  user_email,
                  user_address,
                  user_bank,
                  prize_bonus_amount,
                  prize_money_amount,
                  prize_item_title
                  FROM queue_prize qp
                    LEFT JOIN prize_money pm ON qp.queue_prize_prize_id = pm.prize_money_id
                    LEFT JOIN prize_bonus pb ON qp.queue_prize_prize_id = pb.prize_bonus_id
                    LEFT JOIN prize_item pi ON qp.queue_prize_prize_id = pi.prize_item_id
                  INNER JOIN user u ON u.user_id = qp.queue_prize_user_id
                  WHERE queue_prize_queue_status_id IN (:status_op, :status_pr)";
        $rows = $this->db->queryPrepare($query, array(
            'status_op' => self::STATUS_NEW,
            'status_pr' => self::STATUS_PROCESS,
        ))->fetchAssocAll();
        if(empty($rows)) return array(
            'message' => 'The queue is empty'
        );

        $crud = new CRUD('queue_prize');
        foreach ($rows as $row) {
            $status = ($this->delivery($row)) ? 3 : 2;
            $crud->update($row['queue_prize_id'], array(
                'queue_prize_queue_status_id' => $status
            ));
        }
        return array(
            'message' => array(
                'message' => 'The queue is finally'
            )
        );
    }

    public function delivery($row) {
        $this->toEmail($row['user_email']);
        switch ($row['queue_prize_prize_type']) {
            case PrizeBase::TYPE_MONEY:
                $r = $this->toBank($row['user_bank']);
                break;
            case PrizeBase::TYPE_ITEM:
                $r = $this->toAddress($row['user_address']);
                break;
            default:
                $r = true;
        }
        return $r;
    }

    public function toBank($uri) {
        // Some sending by file_get_contents | cURL
        return true;
    }

    public function toAddress($address) {
        // Some procedure for delivery boy
    }

    public function toEmail($email) {
        // Some sending by smtp
        return true;
    }
}