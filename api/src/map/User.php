<?php


namespace map;


use app\storage\StorageSession;

class User extends AbstractMap
{
    protected $user_id;

    public function selfRead()
    {
        if (!$this->user_id) return array(
            'message' => 'Please login'
        );

        return $this->read('user', $this->user_id);
    }

    public function selfUpdate()
    {
        if (!$this->user_id) return array(
            'message' => 'Please login'
        );

        return $this->update('user', $this->user_id);
    }

    public function login()
    {
        if ($this->user_id) return array(
            'message' => 'You are already logged in'
        );

        $query = 'SELECT user_id FROM user WHERE user_login = :login AND user_password = :password';
        $row = $this->db->queryPrepare($query, gPost())->fetchNum();
        if (!$row) return array(
            'message' => 'Incorrect login or password'
        );

        StorageSession::newId();
        StorageSession::set('user_id', $row[0]);

        return true;
    }

    public function logout()
    {
        if (!$this->user_id) return array(
            'message' => 'Please login'
        );

        StorageSession::destroy();
        return true;
    }

    public function ping() {
        return true;
    }

    public function pong() {
        return false;
    }
}