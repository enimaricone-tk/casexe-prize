<?php


namespace map;

use app\database\CRUD;
use prize\PrizeBase;
use prize\PrizeBonus;

class UserPrize extends AbstractMap
{
    public function checkGetting() {
        $query = "SELECT queue_prize_id, queue_prize_prize_type, prize_money_amount
                  FROM queue_prize qp
                  LEFT JOIN prize_money pm ON qp.queue_prize_prize_id = pm.prize_money_id
                    AND qp.queue_prize_prize_type = :type_money
                  WHERE queue_prize_queue_status_id NOT IN (:status_id, :status_id_ex)
                  AND queue_prize_user_id = :user_id";

        return $this->db->queryPrepare($query, array(
            'user_id' => $this->user_id,
            'status_id' => QueuePrize::STATUS_RETURN,
            'status_id_ex' => QueuePrize::STATUS_EXCEPTION,
            'type_money'=> PrizeBase::TYPE_MONEY,
        ))->fetchAssoc();
    }

    public function get() {
        if(!empty($this->checkGetting())) return array(
            'message' => 'You have already received a prize'
        );

        $arr = PrizeBase::ARR_TYPE;
        $i = array_rand($arr);
        $type = $arr[$i];

        /**
         * @var PrizeBase $prize
         */
        $prize = new $type;
        $r = $prize->generate();
        if(!$r) $r = $this->get();

        return $r;
    }

    public function exchange() {
        $row = $this->checkGetting();
        if(empty($row) || ($row['queue_prize_prize_type'] != PrizeBase::TYPE_MONEY)) return array(
            'message' => 'You have no prizes to exchange'
        );

        $this->refund();

        $amount = $row['prize_money_amount'] * $this->storage->get('prize/factor-money');

        $bonus = new PrizeBonus();
        $new_id = $bonus->createBonus($amount);
        $bonus->addRelation($new_id, PrizeBonus::TYPE_BONUS);

        return array(
            'message' => 'You got bonuses (%s)',
            'prepare' => [$amount]
        );

    }

    public function refund() {
        $row = $this->checkGetting();
        if(empty($row)) return array(
            'message' => 'You have no prizes to refund'
        );

        $crud = new CRUD('queue_prize');
        return (bool)$crud->update($row['queue_prize_id'], array(
            'queue_prize_queue_status_id' => QueuePrize::STATUS_RETURN
        ));
    }
}