<?php


namespace app\database;

use app\storage\Storage;

class CRUD
{
    protected $table;
    protected $storage;

    /**
     * @var DatabaseInterface
     */
    protected $db;

    public function __construct($table)
    {
        $this->storage = Storage::getStorage();
        $this->db = $this->storage->get('db');
        $this->setTable($table);
    }

    public function setTable($table){
        $this->table = $table;
    }

    public function create($data)
    {
        $query = "INSERT INTO {$this->table} ";
        $query .= '(' . implode(array_keys($data), ', ') . ')';
        $query .= ' VALUES (' . implode(array_fill(0, sizeof($data), '?'), ', ') . ')';
        return $this->db->queryPrepare($query, array_values($data));
    }

    public function read($id)
    {
        return $this->actionById(function ($key) use ($id) {
            $query = "SELECT * FROM {$this->table} WHERE {$key} = :_val";
            return $this->db->queryPrepare($query, array(
                '_val' => $id
            ))->fetchAssoc();
        });
    }

    protected function actionById($cb)
    {
        $key = $this->getPrimary();
        if ($key) return $cb($key);

        return array(
            'message' => 'Table %s not found',
            'prepare' => [$this->table]
        );
    }

    protected function getPrimary()
    {
        $config = $this->storage->get('db/config');

        $query = "SELECT COLUMN_NAME FROM information_schema.KEY_COLUMN_USAGE k
                  WHERE k.TABLE_NAME = :table_name
                    AND k.TABLE_SCHEMA = :table_schema
                    AND k.CONSTRAINT_NAME = 'PRIMARY'";
        $row = $this->db->queryPrepare($query, array(
            'table_name' => $this->table,
            'table_schema' => $config['schema']
        ))->fetchNum();

        return ($row) ? $row[0] : false;
    }

    public function update($id, $data)
    {
        return $this->actionById(function ($key) use ($data, $id) {
            $set_arr = [];
            foreach ($data as $k => &$v) $set_arr[] = "{$k}=:{$k}";
            $set_str = implode(', ', $set_arr);

            $data['_val'] = $id;

            $query = "UPDATE {$this->table} SET {$set_str} WHERE $key = :_val";
            return $this->db->queryPrepare($query, $data);
        });
    }

    public function delete($id)
    {
        return $this->actionById(function ($key) use ($id) {
            $query = "DELETE FROM {$this->table} WHERE {$key} = :_val";
            return $this->db->queryPrepare($query, array(
                '_val' => $id
            ));
        });
    }
}