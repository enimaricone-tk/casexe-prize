<?php


namespace app\database;


interface ResultInterface
{
    /**
     * Возвращает результат в виде ассоциативного массива
     *
     * @return array
     */
    public function fetchAssoc();

    /**
     * Возвращает все результаты в виде ассоциативных массивов
     *
     * @return array[]
     */
    public function fetchAssocAll();

    /**
     * Возвращает результат в виде массива с числовыми индексами
     *
     * @return array
     */
    public function fetchNum();

    /**
     * Возвращает все результаты в виде массивов с числовыми индексами
     *
     * @return array[]
     */
    public function fetchNumAll();


    /**
     * Возвращает результат колонки в виде массива с числовыми индексами
     *
     * @param int $column
     * @return array
     */
    public function fetchColumn($column = 0);

    /**
     * Вернуть реальный результат без абстрации
     *
     * @return mixed
     */
    public function getNative();

    /**
     * Возвращает ошибки
     *
     * @return mixed | bool
     */
    public function getError();

    /**
     * Возвращает количество затронутых строк
     *
     * @return int
     */
    public function rowCount();

    /**
     * Возвращает идентификатор созданой записи
     *
     * @return int
     */
    public function newId();
}