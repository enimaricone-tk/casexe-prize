<?php


namespace app\database;


interface DatabaseInterface
{
    /**
     * @param $query
     * @return ResultInterface
     */
    public function query($query);

    /**
     * @param $query
     * @param array $params
     * @return ResultInterface
     */
    public function queryPrepare($query, $params = array());

    public function error();

    public function insertId();

    public function transBegin();

    public function transRollback();

    public function transCommit();
}