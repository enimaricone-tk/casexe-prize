<?php


namespace app\database;


use app\error\Exception;
use app\storage\StorageInterface;

class DatabasePdo implements DatabaseInterface
{
    protected $connect;

    public function __construct(StorageInterface $storage)
    {
        $db = $storage->get('db/config');
        $this->connect = new \PDO($db['dsn'], $db['username'], $db['password']);
    }

    /**
     * @param $query
     * @return ResultPdo
     * @throws Exception
     */
    public function query($query)
    {
        $res = $this->connect->query($query);
        if ($res === false) {
            throw new Exception('Database error: ' . var_export($this->error(), true));
        }
        writeLog($query, 'debug_query');

        return new ResultPdo($res, $this->insertId());
    }

    public function error()
    {
        if (!$this->connect->errorCode()) return false;
        return array(
            'message' => $this->connect->errorInfo(),
            'code' => $this->connect->errorCode()
        );
    }

    public function insertId()
    {
        return $this->connect->lastInsertId();
    }

    /**
     * @param $query
     * @param array $params
     * @return ResultPdo
     * @throws Exception
     */
    public function queryPrepare($query, $params = array())
    {
        $res = $this->connect->prepare($query);
        if ($res === false) {
            $error = $this->error();
            throw new Exception('Database prepare error: ' . var_export($error, true));
        }

        $r = $res->execute($params);
        if ($r === false) {
            $error = $res->errorInfo();
            throw new Exception('Database execute error: ' . var_export($error, true));
        }

        ob_start();
        $res->debugDumpParams();
        $debug = ob_get_clean();
        writeLog($debug, 'debug_queryPrepare');

        return new ResultPdo($res, $this->insertId());
    }

    /**
     * @param $query
     * @return int
     * @throws Exception
     */
    public function exec($query)
    {
        $r = $this->connect->exec($query);
        writeLog($query, 'debug_exec');
        return $r;
    }

    public function transBegin()
    {
        return $this->connect->beginTransaction();
    }

    public function transRollback()
    {
        return $this->connect->rollBack();
    }

    public function transCommit()
    {
        return $this->connect->commit();
    }

    public function getConnect()
    {
        return $this->connect;
    }
}