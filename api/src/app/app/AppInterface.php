<?php


namespace app\app;


interface AppInterface
{
    public static function run($path);
}