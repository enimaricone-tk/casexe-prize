<?php


namespace app\app;

use app\database\DatabasePdo;
use app\storage\Storage;
use app\storage\StorageSession;

class AppOne implements AppInterface
{
    /**
     *
     *
     * @param string $path
     */
    public static function run($path)
    {
        self::entryPoint($path);
    }

    /**
     * Точка входа
     *
     * @param $path
     */
    protected static function entryPoint($path)
    {
        self::beforeRun();
        $id = self::authentication();
        $map = self::authorization($id);
        self::initialization();
        $result = self::routing($path, $map);
        self::exitPoint($result);
    }

    /**
     * Автозагрузка и установка необходимых параметров
     */
    protected static function beforeRun()
    {
        header("Content-type: application/json; charset=utf-8");
    }

    /**
     * Аутентификация
     */
    protected static function authentication()
    {
        $name = StorageSession::SESSION_NAME;
        $key = !empty($_REQUEST[$name]) ? $_REQUEST[$name] : !empty($_COOKIE[$name]) ? $_COOKIE[$name] : 'not-auth';

        StorageSession::sync($key);
        return StorageSession::get('user_id');
    }

    /**
     * Авторизация
     */
    protected static function authorization($user_id)
    {
        if ($user_id) {
            // Определенный пользователь
            $map = array(
                '/self_read/' => array('User', 'selfRead'),
                '/self_update/' => array('User', 'selfUpdate'),
                '/ping/' => array('User', 'ping'),
                '/logout/' => array('User', 'logout'),
                '/prize/get/' => array('UserPrize', 'get'),
                '/prize/exchange/' => array('UserPrize', 'exchange'),
                '/prize/refund/' => array('UserPrize', 'refund')
            );
        } else {
            // Неизвестный | Admin
            $map = array(
                '/login/' => array('User', 'login'),
                '/ping/' => array('User', 'pong'),
                '/:var/create/' => array('Reference', 'create'),
                '/:var/read/:var/' => array('Reference', 'read'),
                '/:var/update/:var/' => array('Reference', 'update'),
                '/:var/delete/:var/' => array('Reference', 'delete'),
                '/queue_process/' => array('QueuePrize', 'process'),
            );
        }
        return $map;
    }

    /**
     * Инициализация необходимых компонентов
     * Можно подстраиваться в зависимости от конфига пользователя
     */
    protected static function initialization()
    {
        $config = require_once DIR_CONFIG . 'storage_config.php';
        $storage = Storage::newStorage($config);

        $db = new DatabasePdo($storage);
        $storage::gSet('db', $db);
    }

    /**
     * Маршрутизация и валидация
     */
    protected static function routing($path, $map)
    {
        $route = self::getRoute($path, $map);
        if ($route) {
            $func = &$route['func'];
            $params = &$route['params'];

            if (is_array($func) && sizeof($func) === 2) {
                $class = 'map\\' . $func[0];
                $func[0] = new $class;
                $result = call_user_func_array($func, $params);
            } else {
                $result = array(
                    'error' => 'Handler not found'
                );
            }
        } else {
            $result = array(
                'message' => 'Route not found (%s)',
                'prepare' => $path
            );
        }
        return $result;
    }

    /**
     * Найти маршрут в карте маршрутов
     *
     * @param $path
     * @param $map
     * @return array|bool
     */
    protected static function getRoute($path, $map)
    {
        $route = &$map[$path];
        $result = false;
        if ($route) {
            $result = array(
                'func' => $route,
                'params' => array()
            );
        } else {
            foreach ($map as $key => $item) {
                if (!strpos($key, ':')) continue;

                $key = '%^' . str_replace(':var', '(.*)', $key) . '$%';
                $res = preg_match($key, $path, $match);
                if ($res) {
                    array_shift($match);
                    $result = array(
                        'func' => $item,
                        'params' => $match
                    );
                }
            }
        }
        return $result;
    }

    /**
     * Точка выхода
     */
    protected static function exitPoint($result)
    {
        if (isset($result['prepare'])) {
            $q = $result['prepare'];
            $m = $result['message'];

            if (is_array($q)) {
                array_unshift($q, $m);
                $params = $q;
            } else {
                $params = array($m, $q);
            }
            unset($result['prepare']);
            $result['message'] = call_user_func_array('sprintf', $params);

        } elseif (is_bool($result)) {
            $result = array(
                'status' => $result
            );
        }
        StorageSession::sync();
        echo json_encode($result, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

}