<?php


namespace app\storage;


class StorageSession
{
    const SESSION_NAME = 'session_key';
    const _KEY_TIME = '_tm';
    const _KEY_STORAGE = '_st';

    protected static $storage = array();
    public static $save_id = 'global';

    public static function get($key)
    {
        return self::$storage[$key];
    }

    public static function set($key, $value)
    {
        self::$storage[$key] = $value;
    }

    public static function sync($id = false)
    {
        self::start($id);

        $st = &$_SESSION[self::_KEY_STORAGE];
        if(!isset($st)) $st = array();

        $time_now = microtime(true);
        if(isset($_SESSION[self::_KEY_TIME]) && $_SESSION[self::_KEY_TIME] > $time_now) {
            $new_data = array_merge($st, self::$storage);
        } else {
            $new_data = array_merge(self::$storage, $st);
        }

        $_SESSION[self::_KEY_TIME] = $time_now;
        self::$storage = $_SESSION[self::_KEY_STORAGE] = $new_data;

        session_write_close();
    }

    public static function newId()
    {
        self::sync(session_create_id());
    }

    public static function destroy() {
        self::start();
        self::$storage = array();
        session_destroy();
    }

    protected static function start($id = false) {
        if(!$id) $id = self::$save_id;
        else self::$save_id = $id;

        session_name(self::SESSION_NAME);
        session_id($id);
        session_start();
    }
}