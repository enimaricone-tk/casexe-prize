<?php


namespace app\storage;

class Storage implements StorageInterface
{
    protected static $dir = DIR_STORAGE;
    protected static $global = array();
    protected static $storageObj = array();
    protected $local = array();
    protected $id;

    private function __construct($config, $id)
    {
        $this->local = $config;
        $this->id = $id;
    }

    public function get($key)
    {
        $r = &$this->local[$key];
        return isset($r) ? $r : self::gGet($key);
    }

    public function set($key, $value)
    {
        $this->local[$key] = $value;
    }

    public static function gSet($key, $value)
    {
        self::$global[$key] = $value;
    }

    public static function gGet($key)
    {
        return self::$global[$key];
    }

    /**
     * Получить хранилище по ключу
     *
     * @param string $key
     * @return Storage|mixed
     */
    public static function getStorage($key = 'default')
    {
        $r = &self::$storageObj[$key];
        return isset($r) ? $r : self::newStorage(array(), $key);
    }

    /**
     * Создать новое хранилище
     *
     * @param array $config
     * @param string $key
     * @return Storage
     */
    public static function &newStorage($config = array(), $key = 'default')
    {
        $r = new static($config, $key);
        self::$storageObj[$key] = $r;
        return $r;
    }


    /**
     * Загрузить глобальное хранилище
     *
     * @param $config
     */
    public static function setGlobalConfig($config)
    {
        self::$global = $config;
    }

}