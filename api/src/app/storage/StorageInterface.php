<?php


namespace app\storage;


interface StorageInterface
{
    public static function gGet($key);

    public static function gSet($key, $value);

    public function get($key);

    public function set($key, $value);
}