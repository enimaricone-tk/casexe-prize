<?php


namespace app\error;


class Error
{
    /**
     * Критическая ошибка, выбрасывем в блок try catch
     *
     * @param array $content
     * @throws Exception
     */
    public static function Critical($content = array())
    {
        throw new Exception($content);
    }

    /**
     * Отправляем сообщение разработчику
     *
     * @param array $content
     */
    public static function Alert($content = array())
    {

    }

    /**
     * Логическая ошибка которая требует исправления, но не мешает выполнению
     *
     * @param array $content
     */
    public static function Syntax($content = array())
    {

    }


    /**
     * Логическая ошибка которая требует вмешательства, но не мешает выполнению
     *
     * @param array $content
     */
    public static function Logic($content = array())
    {

    }

}