<?php


namespace prize;

use map\QueuePrize;

class PrizeItem extends PrizeBase
{
    public function generate()
    {
        $arr = $this->getList();
        if (empty($arr)) return false;

        $i = array_rand($arr);
        $item = $arr[$i];

        $this->addRelation($item['prize_item_id'], self::TYPE_ITEM);

        return array(
            'message' => 'You won a item (%s)',
            'prepare' => [$item['prize_item_title']]
        );
    }

    public function getList()
    {
        $query = "SELECT pi.prize_item_id, prize_item_title FROM prize_item pi
                  LEFT JOIN queue_prize qp ON qp.queue_prize_prize_id = pi.prize_item_id
                    AND qp.queue_prize_prize_type = :type_id
                    AND qp.queue_prize_queue_status_id NOT IN (:status_id_return, :status_id_exception)
                  WHERE qp.queue_prize_id IS NULL";
        return $this->db->queryPrepare($query, array(
            'type_id' => self::TYPE_ITEM,
            'status_id_return' => QueuePrize::STATUS_RETURN,
            'status_id_exception' => QueuePrize::STATUS_EXCEPTION,
        ))->fetchAssocAll();
    }
}