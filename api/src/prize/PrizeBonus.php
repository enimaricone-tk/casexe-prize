<?php


namespace prize;

use app\database\CRUD;

class PrizeBonus extends PrizeBase
{
    public function generate()
    {
        $min = $this->storage->get('prize/bonus-min');
        $max = $this->storage->get('prize/bonus-max');

        $amount = rand($min, $max);
        $new_id = $this->createBonus($amount);
        $this->addRelation($new_id, self::TYPE_BONUS);

        return array(
            'message' => 'You won a bonus (%s)',
            'prepare' => [$amount]
        );
    }

    public function createBonus($amount) {
        $crud = new CRUD('prize_bonus');
        return $crud->create(array(
            'prize_bonus_amount' => $amount
        ))->newId();
    }


}