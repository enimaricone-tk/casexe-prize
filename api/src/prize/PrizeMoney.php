<?php


namespace prize;

use app\database\CRUD;
use map\QueuePrize;

class PrizeMoney extends PrizeBase
{
    public function generate()
    {
        $max = $this->storage->get('prize/money') - $this->getSumAmount();
        if (!$max) return false;

        $amount = rand(1, $max);

        $crud = new CRUD('prize_money');
        $money_id = $crud->create(array(
            'prize_money_amount' => $amount
        ))->newId();

        $this->addRelation($money_id, self::TYPE_MONEY);

        return array(
            'message' => 'You won a money (%s)',
            'prepare' => [$amount]
        );
    }

    public function getSumAmount() {
        $query = "SELECT SUM(prize_money_amount) FROM queue_prize qp 
                  INNER JOIN prize_money pm ON qp.queue_prize_prize_id = prize_money_id
                  WHERE qp.queue_prize_queue_status_id NOT IN (:status_re, :status_ex)
                  AND queue_prize_prize_type = :type_id";
        $row = $this->db->queryPrepare($query, array(
            'type_id' => self::TYPE_MONEY,
            'status_re' => QueuePrize::STATUS_RETURN,
            'status_ex' => QueuePrize::STATUS_EXCEPTION,
        ))->fetchNum();
        return $row[0];
    }
}