<?php


namespace prize;


use app\database\CRUD;
use app\database\DatabaseInterface;
use app\storage\Storage;
use app\storage\StorageSession;
use map\QueuePrize;

abstract class PrizeBase
{
    const TYPE_BONUS = 1;
    const TYPE_MONEY = 2;
    const TYPE_ITEM = 3;

    const ARR_TYPE = [
        PrizeBonus::class,
        PrizeMoney::class,
        PrizeItem::class
    ];

    /**
     * @var DatabaseInterface
     */
    protected $db;
    protected $storage;
    protected $user_id;

    public function __construct()
    {
        $this->storage = Storage::getStorage();
        $this->db = $this->storage->get('db');
        $this->user_id = StorageSession::get('user_id');
    }

    public function checkGetting() {
        $query = "SELECT queue_prize_id, queue_prize_prize_type FROM queue_prize 
                  WHERE queue_prize_queue_status_id != (:status_id, :status_id_ex)
                  AND queue_prize_user_id = :user_id";

        return $this->db->queryPrepare($query, array(
            'user_id' => $this->user_id,
            'status_id' => QueuePrize::STATUS_RETURN,
            'status_id_ex' => QueuePrize::STATUS_EXCEPTION
        ))->fetchAssoc();
    }

    public function addRelation($idPrize, $idType) {
        $crud = new CRUD('queue_prize');

        return $crud->create(array(
            'queue_prize_user_id' => $this->user_id,
            'queue_prize_prize_type' => $idType,
            'queue_prize_prize_id' => $idPrize,
        ))->newId();
    }

    public abstract function generate();
}