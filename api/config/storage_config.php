<?php

return array(
    'prize/bonus-min' => 50,
    'prize/bonus-max' => 1000,
    'prize/money' => 100000,
    'prize/factor-money' => 5,
    'db/config' => DB_CONFIG
);