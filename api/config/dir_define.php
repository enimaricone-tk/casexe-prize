<?php
define('DIR_CONFIG', DIR_ROOT . 'config/');
define('DIR_LOGS', DIR_ROOT . 'logs/');
define('DIR_SOURCE', DIR_ROOT . 'src/');
define('DIR_STORAGE', DIR_ROOT . 'tmp/storage/');

define('DIR_APP', DIR_SOURCE . 'app/');
define('DIR_HELPER', DIR_SOURCE . 'helper/');
define('DIR_MAP', DIR_SOURCE . 'map/');
