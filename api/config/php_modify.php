<?php

set_error_handler(function ($errno, $errstr, $errfile, $errline) {
    $path = DIR_LOGS . "ERROR_" . date("Y-m-d") . ".log";
    file_put_contents($path, PHP_EOL . var_export(array(
            $errno, $errstr, $errfile, $errline
        ), true) . PHP_EOL, FILE_APPEND);
});

set_exception_handler(function (Throwable $e) {
    $path = DIR_LOGS . "CRITICAL_" . date("Y-m-d") . ".log";
    file_put_contents($path, PHP_EOL . var_export(array(
            $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine(),
            $e->getTrace()
        ), true) . PHP_EOL, FILE_APPEND);

    echo json_encode(array(
        'success' => false,
        'message' => 'Server critical exception',
        'code' => 500
    ), JSON_UNESCAPED_UNICODE);
    exit(1);
});

spl_autoload_register(function ($class) {
    $path = realpath(DIR_SOURCE . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php');
    if (file_exists($path)) require_once $path;
});